#!/bin/bash
#+++++++++++++++++++
## Description: This script builds the local dns-server based on bind9
## Author: Ganescu Theodor
## Requirements:
##   - docker

set -o nounset
set -o errexit
set -o errtrace
set -o pipefail

_exit_1() {
    {
        printf "%s " "$(tput setaf 1)!$(tput sgr0)"
        "${@}"
    } 1>&2
    exit 1
}

_log() {
    m_time=$(date "+%F %T")
    echo $m_time" "$1
}
_return_1() {
    {
        printf "%s " "$(tput setaf 1)!$(tput sgr0)"
        "${@}"
    } 1>&2
    return 1
}

# Set $IFS to only newline and tab.
IFS=$'\n\t'

# POSIX way to get script's dir: https://stackoverflow.com/a/29834779/12156188
SCRIPT_DIR="$(cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P)"
CONTAINER_NAME="nameserver"
CONTAINER_TAG="v1"

_log "Delete any running/paused Docker container to avoid overlapping"
docker rm --force nameserver || _log "Container doesn't exist"

_log "Build Docker image to run terraform tests"
docker build --tag ${CONTAINER_NAME}:${CONTAINER_TAG} ${SCRIPT_DIR}/bind9

docker run -ti --rm --privileged --tmpfs /tmp --tmpfs /run \
    --name=bind9 \
    --publish 53:53/udp \
    --publish 53:53/tcp \
    ${CONTAINER_NAME}:${CONTAINER_TAG}
