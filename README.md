<!-- BEGIN_TF_DOCS -->
# Terraform

<Terraform bind9 DNS updater module>

## Usage
Run tests/build-and-run.sh, the DNS server should build and start up automaticaly
Run "terraform apply" and observe the records being created
Add a custom JSON file in the "input-json" folder and run "terraform apply" agin
The new record is being addded
Test the new record : dig newrecord.example.local @127.0.0.1
Expected output :  
 ;; ANSWER SECTION:
  newrecord.example.local. 120	IN	A	192.168.100.201

### Quick Example

```hcl
module "dns_" {
  source = ""
  input1 = <>
  input2 = <>
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.5 |
| <a name="requirement_dns"></a> [dns](#requirement\_dns) | >= 3.2.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_dns"></a> [dns](#provider\_dns) | 3.2.3 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [dns_a_record_set.experimental](https://registry.terraform.io/providers/hashicorp/dns/latest/docs/resources/a_record_set) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `string` | `"local"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->