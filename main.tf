/**
* # Terraform
*
* <Terraform bind9 DNS updater module>
*
*
* ## Usage
* Run tests/build-and-run.sh, the DNS server should build and start up automaticaly
* Run "terraform apply" and observe the records being created
* Add a custom JSON file in the "input-json" folder and run "terraform apply" agin
* The new record is being added
* Test the new record : dig newrecord.example.local @127.0.0.1
* Expected output :  
*  ;; ANSWER SECTION:
*   newrecord.example.local. 120	IN	A	192.168.100.201
* 
* ### Quick Example
*
* ```hcl
* module "dns_" {
*   source = ""
*   input1 = <>
*   input2 = <>
* } 
* ```
*
*/
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---------------------------------------------------------------------------------------------------------------------
# SET TERRAFORM RUNTIME REQUIREMENTS
# ---------------------------------------------------------------------------------------------------------------------

terraform {
  # This module has been updated with 0.12 syntax, which means it is no longer compatible with any versions below 0.12.
  # This module is now only being tested with Terraform 0.13.x. However, to make upgrading easier, we are setting
  # 0.12.26 as the minimum version, as that version added support for required_providers with source URLs, making it
  # forwards compatible with 0.13.x code.
  required_version = ">= 0.13.5"
  required_providers {
    dns = {
      source = "hashicorp/dns"
      version = ">= 3.2.3" 
    }
  }
}

provider "dns" {
  update {
    server = "127.0.0.1"
  }
}

# ------------------------------------------
# Write your local resources here
# ------------------------------------------
# Getting names from filenames

locals {
  json_files = fileset("${path.module}/input-json", "*.json")
}

# ------------------------------------------
# Write your Terraform resources here
# ------------------------------------------

resource "dns_a_record_set" "experimental" {
  for_each = toset(local.json_files)
  zone = jsondecode(file("${path.module}/input-json/${each.value}"))["zone"]
  name = "${element(split(".", "${each.value}"),0)}"
  addresses = jsondecode(file("${path.module}/input-json/${each.value}"))["addresses"]
  ttl = jsondecode(file("${path.module}/input-json/${each.value}"))["ttl"]
}
